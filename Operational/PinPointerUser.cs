﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Operational
{
    public class PinPointerUser
    {
        /// <summary>
        /// The name of the user.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// The last name of the user.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// The occupation of the user.
        /// </summary>
        public string Occupation { get; set; }


    }
}
