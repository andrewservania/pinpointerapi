﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinPointer.ConsoleTester
{
    class Program
    {
        static void Main(string[] args)
        {
            RetrieveUsers();
            Console.ReadKey();
        }

        public static async void RetrieveUsers()
        {
            APICaller caller = new APICaller();
            var response = await caller.GetFreelancerList();

            Console.WriteLine("Users retrieved:\n");
            foreach (var item in response)
            {
                Console.WriteLine("{0} {1}", item.FirstName, item.LastName);
            }
        }
    }
}
