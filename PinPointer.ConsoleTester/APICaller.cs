﻿

using Newtonsoft.Json;
using Operational;
/// Andrew Servania
/// 13 Nov 2016
/// 
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;


namespace PinPointer.ConsoleTester
{
    /// <summary>
    /// Class responsible for doing basic RESTful calls 
    /// to the PinPointerAPI.
    /// </summary>
    public class APICaller
    {
        public APICaller()
        {
        }

        public async Task<List<PinPointerUser>> GetFreelancerList()
        {
            List<PinPointerUser> users = 
                new List<PinPointerUser>();
            using (HttpClient httpClient = new HttpClient())
            {
                try
                {
                    HttpResponseMessage response = await httpClient.GetAsync("http://localhost:3011/Requests/GetFreelancers");

                        string contentString = await response.Content.ReadAsStringAsync();
                        users = JsonConvert.DeserializeObject<List<PinPointerUser>>(contentString);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("No connection can be made with the PinPointerAPI. Make sure the API is running. Exception: {0}",ex);
                }
            }

            return users;
        }
    }
}
