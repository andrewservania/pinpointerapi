﻿using Newtonsoft.Json;
using PinPointerBackend.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PinPointerBackend.Controllers
{
    public class RequestsController : Controller
    {

        private PinPointerEntityModel db = new PinPointerEntityModel();
        //
        // GET: /Requests/
        public ActionResult Index()
        {
            return View();
        }

        // POST: /Requests/GetFreelancers
        [HttpGet]
        [AllowAnonymous]
        public string GetFreelancers()
        { 
            List<PinPointerUserEntity> users = db.PinPointerUserEntities.ToList();

            string jsonString = JsonConvert.SerializeObject(users);

            return jsonString;
        
        }

	}
}