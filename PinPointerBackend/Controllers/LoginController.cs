﻿using PinPointerBackend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace PinPointerBackend.Controllers
{
    public class LoginController : Controller
    {


        //TODO: Investigate what exactly these two properties do.
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();

            }
            private set
            {
                _userManager = value;
            }
        }


        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        public LoginController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }


        /// <summary>
        /// POST method responsible for loging users in via their PinPointer mobile apps,
        /// after conducting the minimum authentication requirements.
        /// </summary>
        /// <param name="user"> The user to-be logged in.</param>
        /// <returns> An HttpStatusCode 'Accepted' if login was succesful otherwise, BadRequest or NotImplemented.</returns>
        public async Task<HttpStatusCode> Login(LoginViewModel user)
        {
            if(!ModelState.IsValid)
                return HttpStatusCode.BadRequest;

            var result = await SignInManager.PasswordSignInAsync(user.Email,user.Password,user.RememberMe, shouldLockout: false);

            switch (result)
            {
                case SignInStatus.Success:
                    return HttpStatusCode.Accepted;
                case SignInStatus.LockedOut:
                    return HttpStatusCode.BadRequest;
                case SignInStatus.RequiresVerification:
                    return HttpStatusCode.NotImplemented; //TODO: Implement this or remove this in the future!
                case SignInStatus.Failure:
                    return HttpStatusCode.BadRequest;
                default:
                    ModelState.AddModelError("","Invalid login attempt.");            
                    return HttpStatusCode.BadRequest;
            }

             
        }
    }
}