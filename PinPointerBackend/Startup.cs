﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PinPointerBackend.Startup))]
namespace PinPointerBackend
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            DatabaseManager databaseManager = new DatabaseManager();
        }
    }
}
