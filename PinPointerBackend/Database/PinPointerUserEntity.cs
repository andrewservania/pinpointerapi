﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PinPointerBackend.Database
{
    public class PinPointerUserEntity
    {
        public PinPointerUserEntity() : this(Guid.NewGuid())
        {
        }

        public PinPointerUserEntity(Guid instanceIdentifier)
        {
            InstanceIdentifier = instanceIdentifier;
        }
        [Key]
        public Guid InstanceIdentifier { get; private set; }
        /// <summary>
        /// The first name of the user.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// The last name of the user.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// The occupation of the user.
        /// </summary>
        public string Occupation { get; set; }
    }
}