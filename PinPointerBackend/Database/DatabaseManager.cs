﻿/// Andrew Servania
/// 13 Nov 2016

using PinPointerBackend.Database;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace PinPointerBackend
{
    /// <summary>
    /// Responsible for doing CRUD operations
    /// towards the PinPointer Database.
    /// </summary>
    public class DatabaseManager
    {
        public DatabaseManager()
        {
            try
            {
                using (var dbContext = new PinPointerEntityModel())
                {
                    var users = dbContext.PinPointerUserEntities;

                    PinPointerUserEntity user = users.SingleOrDefault(item => item.FirstName == "Andrew");

                    if (user == null)
                    {
                        AddSampleUsersToDatabase();
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public void AddSampleUsersToDatabase()
        {
            PinPointerUserEntity user1 = new PinPointerUserEntity()
            {
                FirstName = "Andrew",
                LastName = "Servania",
                Occupation = "Computer Repair Guy"
            };

            PinPointerUserEntity user2 = new PinPointerUserEntity()
            {
                FirstName = "Herbert",
                LastName = "Andersson",
                Occupation = "Cake Baker"
            };

            try
            {
                using (var dbContext = new PinPointerEntityModel())
                {
                    dbContext.PinPointerUserEntities.Add(user1);
                    dbContext.PinPointerUserEntities.Add(user2);
                    dbContext.SaveChanges();
                }
            }
            catch (DbEntityValidationException ex)
            {
                Debug.WriteLine("Could not save changes to database. Exception: ",
                    ex);
            }
        }
    }
}